import java.util.ArrayList;
import java.util.Random;

/**
 * Teleport room for Zuul game
 *
 * Extends room class and overrides getExit(), getLongDescription() method
 *
 * @author Jonas Wedin
 * @version 2014-12-02
 *
 */

public class TeleportRoom extends Room {

    private ArrayList<Room> rooms = new ArrayList<Room>();

    /**
     * Constructor , just calls super constructor
     */
    public TeleportRoom (String description) {
        super(description);
    }

    /**
     * Overrides get exit from base class, returns a random room
     * from array
     * @param direction (does not matter in this case)
     * @return random room
     */
    public Room getExit(String direction) {
        return getRandomRoom();
    }

    /**
     * Overrides method in base class and returns a custom long description
     * @return String description
     */
    public String getLongDescription(){
        return "You are " + super.getShortDescription() + "\n" + "Exits: Anywhere you want";
    }

    /**
     * Add room to rooms list
     * @param room
     */
    public void addRoomToList(Room room) {
        if(room != null) {
            rooms.add(room);
        }
    }

    /**
     * Help function to get a random room
     * @return
     */
    private Room getRandomRoom(){
        Random random = new Random();
        //generate a random index
        int randomIndex = random.nextInt(rooms.size());

        return rooms.get(randomIndex);
    }

}