/**
 *  Beamer class for Zuul game, extends Item class because it needs
 *  to save a room and return that room
 *
 *  @author Jonas Wedin
 *  @version 2014-12-01
 */

public class Beamer extends Item {

    private Room savedRoom = null; // the saved room

    /**
     * Constructor for Beamer, only sends to params to superclass Item
     *
     * @param name
     * @param description
     * @param weight
     * @param canBePickedUp
     */
    public Beamer (String name, String description, int weight, Boolean canBePickedUp) {
        super(name,description,weight,canBePickedUp);

    }

    /**
     * Save a room to memory
     * @param roomToSave
     */
    public void setRoom(Room roomToSave) {
        savedRoom = roomToSave;
    }

    /**
     * Get the saved room
     * @return  The saved room
     */
    public Room getRoom() {
        return savedRoom;
    }
}