/**
 *Colors to be used with terminal
 *
 */

public enum TerminalColor {
    // resets the terminal
    RESET("\u001B[0m"),
    // sets the colors
    BLACK("\u001B[30m"),
    RED("\u001B[31m"),
    GREEN("\u001B[32m"),
    YELLOW("\u001B[33m"),
    BLUE("\u001B[34m"),
    PURPLE("\u001B[35m"),
    CYAN("\u001B[36m"),
    WHITE("\u001B[37m");

    private String colorString;

    /**
     * Constructor
     * @param colorString
     */
    TerminalColor(String colorString) { this.colorString = colorString;}

    /**
     *@return the color code
     */
    public String toString() {return colorString;}


}