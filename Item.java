import java.util.HashMap;
import java.lang.Runnable;

/**
 * Item class for Zuul adventure game
 *
 * An item class, every item has basic things : weight , name , description
 * It also holds and HashMap that can be loaded with functions
 * that trigger to different events
 *
 * @author Jonas Wedin
 * @version 2014-11-25
 */

public class Item {
	private String name;
	private String description;
	private int	weight;
	private Boolean canBePickedUp;
	private Boolean isUseableItem = false;
	private HashMap<String,Runnable> actions = new HashMap<String, Runnable>();
	private HashMap<String,String> triggers = new HashMap<String, String>();

	/**
	 * Constructor for items with just description and weight
	 * @param  description a short description of the item
	 * @param  weight      the weight of the item in grams
	 * 
	 */
	public Item(String name, String description, int weight, Boolean canBePickedUp) {
		this.name = name;
		this.description = description;
		this.weight = weight;
		this.canBePickedUp = canBePickedUp;
	}

	/**
	 * sets the trigger to run an action when triggered
	 * @param triggerKeyword	the trigger keyword
	 * @param actionKeyword		the keyword for the action
	 */
	public void addTrigger(String triggerKeyword, String actionKeyword) {
		triggers.put(triggerKeyword,actionKeyword);
	}

	/**
	 * remove a trigger ( for one-time-things etc)
	 * @param triggerKeyword	the word to trigger with
	 */
	public void removeTrigger(String triggerKeyword) {
		triggers.remove(triggerKeyword);
	}
	/**
	 * returns a specific trigger with a trigger keyword
	 * @param	triggerKeyword	trigger keyword
	 * @return		returns the actionfunction
	 */
	public Runnable getTrigger(String triggerKeyword) {
		Runnable currentAction = actions.get(triggers.get(triggerKeyword));
		return currentAction;
	}

	/**
	 * Returns the action with string index action
	 * @param action	the action to get
	 * @return	the action function
	 */
	public Runnable getAction(String action){
		Runnable currentAction = actions.get(action);
		return currentAction;
	}

	/**
	 * adds an action to be trigger later
	 * @param name		name of the trigger
	 * @param function	function to be triggered
	 */
	public void addAction(String name, Runnable function) {
		actions.put(name, function);
	}

	/**
	 * Returns the name of the item
	 * @return the name of the item
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the description of the item
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the weight
	 * @return weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * Returns true if the item can be picked up, otherwise false
	 * @return Boolean
	 */
	public Boolean getCanBePickedUp(){
		return canBePickedUp;
	}

	/**
	 * Checks if the item is usable
	 * @return	true / false
	 */
	public Boolean isUsableItem() { return isUseableItem; }

	/**
	 * Sets the item to be usable
	 */
	public void setToUsableItem() { isUseableItem = true; }
}