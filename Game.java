import java.lang.Runnable;
import java.util.HashMap;

/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kolling and David J. Barnes and Jonas Wedin
 * @version 2014-12-01
 */

public class Game 
{
    private Parser parser;
    private Player player;
    private OutputBuffer buffer;
    // for use inside the anonymous trigger functions
    private Boolean winCon = false;

    /**
     * Run from command line
     * @param args No arguments defined
     */
    public static void main(String[] args) {
        Game newGame = new Game();
        //if we got --color from command line we use a color output
        for (String arg : args){
            if(arg.equals("--color")){
                newGame.useColor();
            }
        }
        //done with args
        newGame.play();
    }

    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        //create new player
        player = new Player("Jonas", 10000);
        createRooms();
        parser = new Parser();
        buffer = new OutputBuffer();
    }

    /**
     * tell the buffer to use color;
     */
    private void useColor(){
        buffer.useColor();
    }
    /**
     * Create all game components
     *
     * Uses anonymous classes for triggers on items, currently implemeted trigger words are:
     * use, take, help
     * // Important!! //
     * Always name items with capital letter: Key, Teddy , Beamer etc...
     * Always name exits with small letters: north, south, east, up etc...
     *
     */
    private void createRooms()
    {
        //declaring variables as final so they can be used in the anonmyous functions for triggers
        final Room officeCellar, trapRoom, treasureRoom, antidoteRoom, lockedRoom, afterLockedRoom, drFluffyRoom;
        final Item rustyKey, bigTeddyBear, gold,antidote;
        final Beamer beamer;
        final TeleportRoom teleportRoom;
      
        // create the rooms
        officeCellar = new Room("in a creepy cellar, it smells damp. On the wall \nbehind you it's written in crayon: \nTake the bear with you..");
        trapRoom = new Room("in a small dark room. And the door locks behind you! you can't go back!");
        trapRoom.setToTrapRoom();
        treasureRoom = new Room("in a big shiny hall, filled with treasures. \nTo the west you see a teleport room!");
        antidoteRoom = new Room("in small room");
        lockedRoom = new Room("in a hallway , you see a old rusty door but it's locked.");
        afterLockedRoom = new Room("in a ordinary room. You see a staircase!");
        drFluffyRoom = new Room("in the exit to the basement! But Dr.Fluffy is blocking the way, he says:\n''Dr fluffy need he's cuddely bear, if you have teddy drop him and \ni will let you go' '");
        teleportRoom = new TeleportRoom("in a mysterious room with " + TerminalColor.GREEN + "s" + TerminalColor.RED + "h" + TerminalColor.YELLOW + "i" + TerminalColor.CYAN + "m" + TerminalColor.PURPLE + "mering" + TerminalColor.RESET + " walls. \na text on the wall says: Leave here and you will end up somewhere random \n");


        //add rooms to teleport room
        teleportRoom.addRoomToList(officeCellar);
        teleportRoom.addRoomToList(trapRoom);
        teleportRoom.addRoomToList(treasureRoom);
        teleportRoom.addRoomToList(antidoteRoom);
        teleportRoom.addRoomToList(lockedRoom);
        teleportRoom.addRoomToList(afterLockedRoom);
        teleportRoom.addRoomToList(drFluffyRoom);


        //create items
        rustyKey = new Item("Key", "An old rusty key", 100, true);
        rustyKey.addAction("use" , new Runnable() {
                public void run() {
                    if(player.getCurrentRoom() == lockedRoom) {
                        buffer.addOutput("You've unlocked the old door", TerminalColor.GREEN);
                        lockedRoom.setExit("north", afterLockedRoom);
                        lockedRoom.changeDescription("in a hallway");
                        player.destroyItem(rustyKey.getName());
                        //print room description
                        buffer.addOutput(player.getCurrentRoom().getLongDescription());
                    } else {
                        buffer.addOutput("You can't use that here", TerminalColor.RED);
                    }
                }
        } );
        rustyKey.setToUsableItem();

        gold = new Item("Gold", "A loot of flashy gold, with this you can buy anything!", 2000, true);
        gold.addAction("takeAction", new Runnable() {
                public void run() {
                    player.poisonPlayer();
                    gold.removeTrigger("take");

                }
        } );
        gold.addTrigger("take", "takeAction");

        bigTeddyBear = new Item("Teddy", "A cute cuddely fluffy big papa bear!", 800,true);
        bigTeddyBear.setToUsableItem();
        bigTeddyBear.addAction("use", new Runnable() {
                public void run() {
                    buffer.addOutput("You press the teddys tummy and it says:");
                    buffer.addOutput("--Teddy bear loves you! *kiss*--", TerminalColor.CYAN);
                    buffer.addOutput("in a really dark and scary voice...");
                }
        } );
        bigTeddyBear.addAction("dropAction", new Runnable() {
                public void run() {
                    if(player.getCurrentRoom() == drFluffyRoom){
                        buffer.addOutput("Dr fluffy says:");
                        buffer.addOutput("'Thaank you ! You may leave", TerminalColor.CYAN);
                        drFluffyRoom.changeDescription("in the exit to the basement! Dr. Fluffy is showing the \nway out!");
                        buffer.addOutput(player.getCurrentRoom().getLongDescription());
                        bigTeddyBear.removeTrigger("drop");
                        //exit the game
                        winCon = true;
                    }
                }
        } );
        bigTeddyBear.addTrigger("drop","dropAction");

        antidote = new Item("Antidote", "Super-Can-Cure-Anything-Drink", 10, true);
        antidote.setToUsableItem();
        antidote.addAction("use", new Runnable() {
                public void run() {
                    if(player.isPoisoned()) {
                        player.curePlayer();
                        buffer.addOutput("You drink the all of the  antidote and feel a slight tingeling sensation..");
                        buffer.addOutput("You're cured of anything you had!", TerminalColor.GREEN);
                        buffer.addOutput("The bottle mysteriously vanishes...");
                        player.destroyItem(antidote.getName());
                    } else {
                        buffer.addOutput("You taste the antidote ... seems like medicine");
                    }

                }
        } );

        beamer = new Beamer("Beamer", "A cool gun, fire it once to remember a room, again to teleport to that room", 2000, true);
        beamer.setToUsableItem();
        beamer.addAction("use", new Runnable() {
                public void run() {
                    if(beamer.getRoom() == null){
                        buffer.addOutput("KABOOOM! Saving current room in memory!", TerminalColor.PURPLE);
                        beamer.setRoom(player.getCurrentRoom());
                    } else {
                        buffer.addOutput("KABOOM! Transporteeeeed!" ,TerminalColor.PURPLE);
                        Room newRoom = beamer.getRoom();
                        changeRoom(newRoom);
                        player.cleanRoomStack();
                        beamer.setRoom(null);

                    }
                }
        } );
        beamer.addAction("printHelp", new Runnable() {
                public void run() {
                    buffer.addOutput("Beamer:");
                    buffer.addOutput("Fire it once to save a room, fire it again to teleport to that room");
                }
        } );
        beamer.addTrigger("help", "printHelp");

        // initialise room exits
        officeCellar.setExit("north", trapRoom);

        trapRoom.setExit("north", treasureRoom);

        treasureRoom.setExit("south", trapRoom);
        treasureRoom.setExit("north", antidoteRoom);
        treasureRoom.setExit("west", teleportRoom);

        antidoteRoom.setExit("south", treasureRoom);
        antidoteRoom.setExit("north", lockedRoom);

        lockedRoom.setExit("south", antidoteRoom);

        afterLockedRoom.setExit("south", lockedRoom);
        afterLockedRoom.setExit("up", drFluffyRoom);

        drFluffyRoom.setExit("down", afterLockedRoom);


        //add items
        officeCellar.addItem(rustyKey.getName(), rustyKey);
        officeCellar.addItem(bigTeddyBear.getName(), bigTeddyBear);
        treasureRoom.addItem(gold.getName(), gold);
        antidoteRoom.addItem(antidote.getName(), antidote);
        afterLockedRoom.addItem(beamer.getName(), beamer);

        //start the game outside
        player.changeRoom(officeCellar);
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();
        buffer.printOutput();
        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.
                
        boolean finished = false;
        while (! finished && player.isAlive() && !winCon) {
            Command command = parser.getCommand();
            finished = processCommand(command);
            //check player status
            checkPoison();
            //printbuffer only if where going to repeat the loop
            if(!finished && player.isAlive() && !winCon) {
                buffer.printOutput();
            }
        }
        if(winCon) {
            buffer.addOutput("You've won the game! Good for ya!", TerminalColor.GREEN);
        }
        buffer.addOutput("Thank you for playing.  Good bye.", TerminalColor.RED);
        buffer.printOutput();
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {

        buffer.addOutput("Welcome to the World of Juul!", TerminalColor.YELLOW);
        buffer.addOutput("You are trapped in Dr.Love-me-da-fluffy-stuff's ");
        buffer.addOutput("basement and you need to get out.");
        buffer.addOutput("Type '" + CommandWord.HELP + "' if you need help.", TerminalColor.BLUE);
        buffer.addOutput(player.getCurrentRoom().getLongDescription());

    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;

        CommandWord commandWord = command.getCommandWord();

        switch (commandWord) {
            case UNKNOWN:
                buffer.addOutput("I don't know what you mean...");
                break;

            case HELP:
                printHelp();
                break;

            case GO:
                goRoom(command);
                break;
                
            case BACK:
                back();
                break;

            case TAKE:
                take(command);
                break;

            case DROP:
                drop(command);
                break;

            case ITEMS:
                items();
                break;

            case USE:
                use(command);
                break;

            case LOOK:
                look();
                break;

            case QUIT:
                wantToQuit = quit(command);
                break;
        }
        return wantToQuit;
    }

    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        buffer.addOutput("You are lost. You are alone. You wander", TerminalColor.YELLOW);
        buffer.addOutput("Your command words are:");
        buffer.addOutput(parser.showCommands());
        HashMap<String, Item> currentItems = player.getItems();
        for (Item item:currentItems.values()) {
            Runnable trigger = item.getTrigger("help");
            if (trigger != null) {
                trigger.run();
            }
        }
    }

    /** 
     * Try to go in one direction. If there is an exit, enter the new
     * room, otherwise print an error message.
     */
    private void goRoom(Command command) 
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            buffer.addOutput("Go where?", TerminalColor.RED);
            return;
        }

        String direction = command.getSecondWord().toLowerCase();

        // Try to leave current room.
        Room nextRoom = player.getCurrentRoom().getExit(direction);

        if (nextRoom == null) {
            buffer.addOutput("There is no door!",TerminalColor.RED);
        }
        else {
            changeRoom(nextRoom);
            //if this is a traproom we clear the current room history
            if(nextRoom.isTrapRoom()){
                player.cleanRoomStack();
            }
        }
    }

    /**
     * Go back to the previous room, uses a stack
     */
    private void back() 
    {
        //attempts to move back one room
        if(player.goBack() == null) {
            buffer.addOutput("You cant go back! There is nothing to go back to! Run!", TerminalColor.RED);
        } else {
            buffer.addOutput(player.getCurrentRoom().getLongDescription());
        }
    }

    /**
     * Change room
     * @param Room the room the change to
     */
    private void changeRoom(Room room)
    {
        player.changeRoom(room);
        //print room description
        buffer.addOutput(player.getCurrentRoom().getLongDescription());
    }

    /**
     * Look command, gives description of the room and items in the room
     */
    private void look() 
    {
        items();
        buffer.addOutput(player.getCurrentRoom().getLongDescription());
    }

    /**
     * allows a user to take an item
     * @param command the command
     */
    private void take(Command command)
    {
        if(!command.hasSecondWord()) {
            buffer.addOutput("Take what?", TerminalColor.RED);
            return;
        }

        String itemName = capitalizeWord(command.getSecondWord());
        Item retrivedItem = player.getCurrentRoom().removeItem(itemName);

        //there was no such item
        if( retrivedItem == null) {
            buffer.addOutput("There is no such item in here...", TerminalColor.YELLOW);
            buffer.addOutput(player.getCurrentRoom().getItemsDescription());
            return;
        } else {
            //we found the item!
            if(player.pickUpItem(retrivedItem)){
                //trigger "take"
                runItemTrigger(retrivedItem, "take");
                buffer.addOutput("You picked up :" + retrivedItem.getName(), TerminalColor.GREEN);
            } else {
                buffer.addOutput("The object was to heavy... weakling!", TerminalColor.RED);
                player.getCurrentRoom().addItem(retrivedItem.getName(),retrivedItem);
            }
        }
    }

    /**
     * Triggers an item trigger using a string
     *
     * @param item
     * @param triggerString
     */
    private void runItemTrigger(Item item, String triggerString) {
        Runnable triggerAction = item.getTrigger(triggerString);
        if(triggerAction != null){
            triggerAction.run();
        }
    }

    /**
     * user drops an item
     * @param command the command passed down
     */
    private void drop(Command command)
    {
        if(!command.hasSecondWord()) {
            buffer.addOutput("Drop what?", TerminalColor.RED);
            return;
        }

        String itemName = capitalizeWord(command.getSecondWord());
        Item droppedItem = player.dropItem(itemName);
        if(droppedItem == null){
            buffer.addOutput("You don't have anything like that!", TerminalColor.YELLOW);
            return;
        } else {
            //trigger drop on items
            runItemTrigger(droppedItem,"drop");
            buffer.addOutput("You've dropped: " + droppedItem.getName(), TerminalColor.GREEN);
            player.getCurrentRoom().addItem(droppedItem.getName(), droppedItem);
        }
    }

    /**
     * If its and usable item it runs the use action , otherwise print message that you cant
     * @param command the command passed down
     */
    private void use(Command command) {
        if(!command.hasSecondWord()) {
            //there was no second command word
            buffer.addOutput("Use what?", TerminalColor.RED);
            return;
        }
        String itemName = capitalizeWord(command.getSecondWord());
        if(!player.playerHasItem(itemName)) {
            //player didnt have the item specified
            buffer.addOutput("You cant use what you don't have...", TerminalColor.YELLOW);
            return;
        }
        Item newItem = player.getItem(itemName);
        if(!newItem.isUsableItem()) {
            //item was not usable
            buffer.addOutput("You cant use " + itemName , TerminalColor.RED);
        } else {
            //run the action saved in item

            newItem.getAction("use").run();
        }
    }

    /**
     * Prints the users current items
     */
    private void items(){
        String currentItems = player.listItemsByName();
        if(currentItems.isEmpty()){
            buffer.addOutput("You're not carrying anything... ", TerminalColor.YELLOW);
        } else {
            buffer.addOutput("You're carrying: " + currentItems);
        }
    }

    /**
     * Check if player is poisoned and buffer the output!
     * todo - Should be more generalised?
     */
    private void checkPoison(){
        buffer.addOutput(player.checkPoison(), TerminalColor.RED);
    }

    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    private boolean quit(Command command) 
    {
        if(command.hasSecondWord()) {
            buffer.addOutput("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }

    /**
     * Help function to easy item input. teddy -> Teddy etc.
     * @param word  the string to work on
     * @return capitalized string
     */
    private String capitalizeWord(String word) {
        if(word.isEmpty()) {
            return word;
        } else if (word.length() <= 1) {
            return word.toUpperCase();
        } else {
            return word.substring(0,1).toUpperCase() + word.substring(1).toLowerCase();
        }
    }
}
