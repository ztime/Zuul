import java.util.ArrayList;

/**
 * An output class for Zuul game.
 * Takes everything you put into it and print pretty lines around it
 *
 * @author Jonas Wedin
 * @version 2014-12-01
 *
 */

public class OutputBuffer {
    private ArrayList<String> buffer; // the lines we want to print
    private String line = "///////////////////////////////////////////////////"; // line before and after output
    private String preString =  "// "; // before each line print this
    private Boolean useColor = false;

    /**
     * Constructor for OutputBuffer, only initiates arraylist
     */
    public OutputBuffer() {
        buffer = new ArrayList<String>();
    }

    public void useColor(){
        this.useColor = true;
    }
    /**
     * Adds a string to buffer, splits it if it contains \n
     * @param output
     */
    public void addOutput(String output) {
        // we dont add an empty string
        if(output.isEmpty()){
            return;
        }
        //we check if string contains linebreaks
        String[] splitString = output.split("\n");
        if(splitString.length > 1) {
            for(String currentString:splitString) {
                //add each line
                buffer.add(currentString);
            }
        } else {
            buffer.add(output);
        }
    }

    /**
     * Adds a string to the buffer and colors it with the color supplied
     * @param output
     * @param color
     */
    public void addOutput(String output, TerminalColor color) {
        //if we dont use color, skip this function
        if(!useColor) {
            addOutput(output);
            return;
        }
        // we dont add an empty string
        if(output.isEmpty()){
            return;
        }

        //we check if string contains linebreaks
        String[] splitString = output.split("\n");
        if(splitString.length > 1) {
            for(String currentString:splitString) {
                //add each line and the color before
                buffer.add(color + currentString + TerminalColor.RESET);
            }
        } else {
            buffer.add(color + output + TerminalColor.RESET);
        }
    }


    /**
     * Print the buffer line by line , with line and pre string
     * doesnt print anything if buffer is empty
     */
    public void printOutput() {
        //dont print anything if buffer is empty
        if(buffer.isEmpty()) {
            return;
        }
        printLine();
        for(String currentLine: buffer) {
            System.out.println(preString + currentLine);
        }
        printLine();
        buffer.clear();
    }

    /**
     * Help function to print top and bottom line
     */
    private void printLine(){
        System.out.println(line);
    }

}