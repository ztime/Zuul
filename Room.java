import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Class Room - a room in an adventure game.
 *
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.  
 *
 * A "Room" represents one location in the scenery of the game.  It is 
 * connected to other rooms via exits.  For each existing exit, the room 
 * stores a reference to the neighboring room.
 * 
 * @author  Michael Kolling and David J. Barnes and Jonas Wedin
 * @version 2011.08.10 and 2014-11-25
 */

public class Room 
{
    private String description;
    private Boolean isTrapRoom;
    private HashMap<String, Room> exits;        // stores exits of this room.
    private HashMap<String, Item> items;        // stores items of the room;


    /**
     * Create a room described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     * @param description The room's description.
     */
    public Room(String description) 
    {
        this.description = description;
        isTrapRoom = false;
        exits = new HashMap<String, Room>();
        items = new HashMap<String, Item>();
    }

    /**
     * Change the current description of the room
     * @param description   the new description
     */
    public void changeDescription(String description) {
        this.description = description;
    }

    /**
     * checks if this room is an trap room
     * @return true / false
     */
    public Boolean isTrapRoom() {
        return isTrapRoom;
    }

    /**
     * Sets this room to a trap room
     */
    public void setToTrapRoom(){
        isTrapRoom = true;
    }

    /**
     * Define an exit from this room.
     * @param direction The direction of the exit.
     * @param neighbor  The room to which the exit leads.
     */
    public void setExit(String direction, Room neighbor) 
    {
        exits.put(direction, neighbor);
    }

    /**
     * Adds an item to the room
     * @param item the item to be added
     */
    public void addItem(String name, Item item) 
    {
        items.put(name,item);
    }

    /**
     * Gets the short desc. of the room
     * @return The short description of the room
     *
     */
    public String getShortDescription()
    {
        return description;
    }

    /**
     * Return a description of the room in the form:
     *     You are in the kitchen.
     *     --/if there is items in the room/--
     *     Your see the following items:
     *     items description
     *     Exits: north west
     * @return A long description of this room
     */
    public String getLongDescription()
    {   

        return "You are " + description + ".\n" + getItemsDescription() + "\n" + getExitString();

    }

    /**
     * Returns a description of all items in the room.
     * @return  a string description
     */
    public String getItemsDescription()
    {
        String itemsDescriptions = "";
        if(items.size() != 0) {
            itemsDescriptions = "You see the following items: ";
            for(Item currentItem : items.values()) {
                itemsDescriptions += "\n" + currentItem.getName() + " : " + currentItem.getDescription() ;
            }
        }
        return itemsDescriptions;
    }

    /**
     * Removes an item from the room returns the removed item or null if it was not found
     * @return the removed item
     */
    public Item removeItem(String key)
    {
        Item returnItem = items.get(key);
        if (returnItem == null) {
            return null;
        } else {
            items.remove(key);
            return returnItem;
        }
    }
    /**
     * Return a string describing the room's exits, for example
     * "Exits: north west".
     * @return Details of the room's exits.
     */
    private String getExitString()
    {
        String returnString = "Exits:";
        Set<String> keys = exits.keySet();
        for(String exit : keys) {
            returnString += " " + exit;
        }
        return returnString;
    }

    /**
     * Return the room that is reached if we go from this room in direction
     * "direction". If there is no room in that direction, return null.
     * @param direction The exit's direction.
     * @return The room in the given direction.
     */
    public Room getExit(String direction) 
    {
        return exits.get(direction);
    }
}

