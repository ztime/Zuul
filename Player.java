import java.util.HashMap;
import java.util.Stack;

/**
 * A player class for zuul game
 *
 * Player can hold items, and contains information about
 * the room there in.
 *
 * @author Jonas Wedin
 * @version 2014-11-25
 */
public class Player {
    private String name;
    private int weight = 0;     //total weight of carried items (in grams)
    private int maxWeight;  // the max weight for player
    private int actionCounter = 0; //counts the actions the user takes
    private Boolean poisoned = false; //if the player is poisoned
    private Boolean isAlive = true; //if the player is alive
    private Room currentRoom;
    private Stack<Room> roomStack;
    private HashMap<String, Item> items;

    /**
     * Constructor for player with name and max weight
     * @param name          name of the player
     * @param maxWeight     maximum amount of weight the player can carry
     */
    public Player(String name, int maxWeight) {
        roomStack = new Stack<Room>();
        items = new HashMap<String, Item>();
        this.maxWeight = maxWeight;
        this.name = name;
    }

    /**
     * Returns true / false if the player is alive
     * @return true / false
     */
    public Boolean isAlive() {
        return this.isAlive;
    }
    /**
     * Returns the current room
     * @return  the current room
     */
    public Room getCurrentRoom() {
        return currentRoom;
    }

    /**
     * move back player one room, if that is not possible we return null
     * (Counts as an action)
     * @return the last room visited
     */
    public Room goBack() {
        tickActionCounter();
        if (roomStack.size() <= 1) {
            //we could not move back
            return null;
        }

        currentRoom = roomStack.pop();
        return currentRoom;
    }

    /**
     * Change room
     * (Counts as an action)
     * @param newRoom the room to change to
     */
    public void changeRoom(Room newRoom) {
        tickActionCounter();
        //push the old one to the stack
        roomStack.push(currentRoom);
        //change room
        currentRoom = newRoom;
    }

    /**
     * if player has the item with name this returns true otherwise false
     * @param name  the item name
     * @return true / false
     */
    public Boolean playerHasItem(String name) {
        if(items.get(name) != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * returns an item
     * @param name  the item name
     * @return  the item
     */
    public Item getItem(String name) {
        return items.get(name);
    }

    /**
     * Returns the entire list of items the player carries
     * @return      An HashMap with all the items the player has
     */
    public HashMap<String, Item> getItems(){
        return items;
    }

    /**
     * Returns true if the item was picked up , returns false if weight is to much
     * (Counts as an action)
     * @param newItem the item to pickup
     * @return true / false
     */
    public Boolean pickUpItem(Item newItem) {
        tickActionCounter();
        if((weight + newItem.getWeight()) > maxWeight) {
            return false;
        }
        // everything is fine, we pickup the item
        items.put(newItem.getName(), newItem);
        weight += newItem.getWeight();
        return true;
    }

    /**
     * Drops an item, returns the item if the item was dropped, null if it does not exist
     * (Counts as an action)
     * @param itemName  the itemName to drop
     * @return true/false
     */
    public Item dropItem(String itemName) {
        tickActionCounter();
        Item item = items.get(itemName);
        if(item == null) {
            // we didn't find the item
            return null;
        } else {
            items.remove(itemName);
            weight -= item.getWeight();
            return item;
        }
    }

    /**
     * Destroys an item the player has
     * @param itemName name of the item
     * @return true / false
     */
    public Boolean destroyItem(String itemName) {
        if(items.get(itemName) != null) {
            items.remove(itemName);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns a string with all the current items by name (empty if there is none)
     * Like this: "item1, item2, item3"
     * (Counts as an action)
     * @return  a string with the items
     */
    public String listItemsByName(){
        tickActionCounter();
        String returnString = "";
        for (Item item : items.values()) {
            returnString += item.getName() + ", ";
        }
        //remove the last comma
        if(returnString.length() > 2) {
            returnString = returnString.substring(0,returnString.length() - 2);
        }
        return returnString;
    }

    /**
     * Cleans a players room stack so that only the current room is in it.
     * use if a player should not be able to move back
     */
    public void cleanRoomStack() {
        roomStack.clear();
        roomStack.push(currentRoom);
    }

    /**
     * Poison the player. Poison class??
     */
    public void poisonPlayer(){
        resetActionCounter();
        poisoned = true;
    }

    /**
     * Check poison if player is dead or how many turns there is left
     * @return
     */
    public String checkPoison(){
        //skip if player is not poisoned
        if(!poisoned){
            return "";
        }

        if(actionCounter >= 5) {
            isAlive = false;
            return "You died from poison...";
        } else {
            return "You are poisoned! You have " + (5 - actionCounter) + " moves \n to find the antidote!";
        }

    }

    /**
     * Cure the player
     */
    public void curePlayer(){
        poisoned = false;
    }

    /**
     * returns if the player is poisoned
     * @return true / false
     */
    public Boolean isPoisoned() {
        return poisoned;
    }

    /**
     * Ticks the action counter
     */
    public void tickActionCounter() {
        actionCounter++;
    }

    /**
     * Returns the current action counter
     * @return the current count
     */
    public int getActionCounter() {
        return actionCounter;
    }

    /**
     * Reset the action counter
     */
    private void resetActionCounter() {
        actionCounter = 0;
    }

}
